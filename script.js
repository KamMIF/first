var fido = {
  name: "Fido",
  weight: 48,
  breed: "Mixed",
  loves: "walks"
};

function loseWeight(dog, amount) {
  dog.weight = dog.weight - amount;
};
loseWeight(fido, 10);
alert(fido.name + " now weights " + fido.weight);
